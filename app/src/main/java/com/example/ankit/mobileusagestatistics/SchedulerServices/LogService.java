package com.example.ankit.mobileusagestatistics.SchedulerServices;

import android.Manifest;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.util.ArrayMap;

import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.databases.LogsDatabaseHelper;
import com.example.ankit.mobileusagestatistics.models.AppUsageLog;
import com.example.ankit.mobileusagestatistics.models.SMSLog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class LogService extends JobService {

    private volatile ServiceHandler mServiceHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread(LogService.class.getName(), Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        mServiceHandler = new ServiceHandler(this, thread.getLooper());
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        mServiceHandler.sendMessage(Message.obtain(mServiceHandler, 1, params));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private final static class ServiceHandler extends Handler {

        private WeakReference<Context> mContextWeakReference;

        public ServiceHandler(Context context, Looper looper) {
            super(looper);
            mContextWeakReference = new WeakReference<>(context);
        }

        @Override
        public void handleMessage(Message msg) {

            Context context = mContextWeakReference.get();

            if (context != null) {
                //get shared preferences
                SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);

                //get last call log time
                long lastCallLogTime = sharedPreferences.getLong(Constants.KEY_LAST_CALL_LOG_TIME, 0);

                //log calls
                addCallLogsInDB(context, lastCallLogTime, sharedPreferences);

                //get last sms log time
                long lastSmsLogTime = sharedPreferences.getLong(Constants.KEY_LAST_SMS_LOG_TIME, 0);

                //log sms
                addSmsLogsInDB(context, lastSmsLogTime, sharedPreferences);

                //get last app usage log time
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.YEAR, -1);
                long lastAppUsageLogTime = sharedPreferences.getLong(Constants.KEY_LAST_APP_USAGE_LOG_TIME, cal.getTimeInMillis());

                //log app usage
                //addAppUsageLogsInDB(context, lastAppUsageLogTime, sharedPreferences);
                getAppServiceLogs(context);

                ((LogService) context).jobFinished((JobParameters) msg.obj, false);
            }
        }
    }

    public static void addCallLogsInDB(Context context, long lastCallLogTime, SharedPreferences sharedPreferences) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG)
                == PackageManager.PERMISSION_GRANTED) {

            String[] projection = new String[]{
                    CallLog.Calls.NUMBER,
                    CallLog.Calls.TYPE,
                    CallLog.Calls.DATE,
                    CallLog.Calls.DURATION
            };
            String selection = android.provider.CallLog.Calls.DATE + " > ?";
            String strOrder = android.provider.CallLog.Calls.DATE + " ASC";

            Cursor cursor = context.getContentResolver()
                    .query(CallLog.Calls.CONTENT_URI, projection, selection,
                            new String[]{String.valueOf(lastCallLogTime)}, strOrder);

            if (cursor != null && cursor.getCount() > 0) {

                int numberColumn = cursor.getColumnIndex(CallLog.Calls.NUMBER);
                int typeColumn = cursor.getColumnIndex(CallLog.Calls.TYPE);
                int dateColumn = cursor.getColumnIndex(CallLog.Calls.DATE);
                int durationColumn = cursor.getColumnIndex(CallLog.Calls.DURATION);

                List<com.example.ankit.mobileusagestatistics.models.CallLog> callLogs = new ArrayList<>(cursor.getCount());

                while (cursor.moveToNext()) {

                    com.example.ankit.mobileusagestatistics.models.CallLog callLog = new com.example.ankit.mobileusagestatistics.models.CallLog();
                    callLog.number = cursor.getString(numberColumn);
                    callLog.type = cursor.getInt(typeColumn);
                    callLog.date = cursor.getLong(dateColumn);
                    callLog.duration = cursor.getLong(durationColumn);

                    callLogs.add(callLog);

                }
                cursor.close();

                LogsDatabaseHelper.getInstance().insertCallLogs(callLogs);

                sharedPreferences.edit().putLong(Constants.KEY_LAST_CALL_LOG_TIME,
                        callLogs.get(callLogs.size() - 1).date).commit();
            }
        }
    }

    public static void addSmsLogsInDB(Context context, long lastSmsLogTime, SharedPreferences sharedPreferences) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED) {

            // Create Sent box URI
            Uri sentURI = Uri.parse("content://sms/sent");

            // List required columns
            String[] projection = new String[]{
                    Telephony.Sms._ID, Telephony.Sms.DATE, Telephony.Sms.ADDRESS, Telephony.Sms.BODY
            };
            String selection = Telephony.Sms.DATE + " > ?";
            String strOrder = Telephony.Sms.DATE + " ASC";

            // Fetch Sent SMS Message from Built-in Content Provider
            Cursor cursor = context.getContentResolver().query(sentURI, projection, selection,
                    new String[]{String.valueOf(lastSmsLogTime)}, strOrder);
            if (cursor != null && cursor.getCount() > 0) {
                int idColumn = cursor.getColumnIndex(Telephony.Sms._ID);
                int dateColumn = cursor.getColumnIndex(Telephony.Sms.DATE);
                int addressColumn = cursor.getColumnIndex(Telephony.Sms.ADDRESS);
                int bodyColumn = cursor.getColumnIndex(Telephony.Sms.BODY);

                List<SMSLog> smsLogs = new ArrayList<>(cursor.getCount());

                while (cursor.moveToNext()) {

                    SMSLog smsLog = new SMSLog();
                    smsLog.id = cursor.getInt(idColumn);
                    smsLog.date = cursor.getInt(dateColumn);
                    smsLog.address = cursor.getString(addressColumn);
                    smsLog.body = cursor.getString(bodyColumn);

                    smsLogs.add(smsLog);

                }
                cursor.close();

                LogsDatabaseHelper.getInstance().insertSmsLogs(smsLogs);

                sharedPreferences.edit().putLong(Constants.KEY_LAST_SMS_LOG_TIME,
                        smsLogs.get(smsLogs.size() - 1).date).commit();
            }
        }
    }

    public static void addAppUsageLogsInDB(Context context, long lastAppUsageLogTime, SharedPreferences sharedPreferences) {


        if (Utils.hasUsageStatisticsPermission(context)) {

            UsageStatsManager usageStatsManager = (UsageStatsManager) context
                    .getSystemService(USAGE_STATS_SERVICE);

            long endTime = System.currentTimeMillis();
            List<UsageStats> queryUsageStats = new ArrayList<>(usageStatsManager.queryAndAggregateUsageStats(lastAppUsageLogTime, endTime).values());

            int size;
            if ((size = queryUsageStats.size()) > 0) {

                List<AppUsageLog> appUsageLogs = new ArrayList<>(size);

                for (int i = 0; i < size; i++) {

                    long foregroundTime = queryUsageStats.get(i).getTotalTimeInForeground() / 1000;
                    String packageName = queryUsageStats.get(i).getPackageName();
                    if (packageName.contains("com.android")
                            || packageName.contains("com.google.android")
                            || packageName.contains("example")
                            || foregroundTime <= 0
                            || queryUsageStats.get(i).getFirstTimeStamp() <= lastAppUsageLogTime) {
                        continue;
                    }

                    AppUsageLog appUsageLog = new AppUsageLog();
                    appUsageLog.packageName = packageName;

                    try {
                        ApplicationInfo app = context.getPackageManager().getApplicationInfo(appUsageLog.packageName, 0);
                        String name = context.getPackageManager().getApplicationLabel(app).toString().toLowerCase();

                        if (Constants.BROWSER_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_BROWSER_APPS;
                        } else if (Constants.SOCIAL_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_SOCIAL_APPS;
                        } else if (Constants.CAB_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_CAB_APPS;
                        } else if (Constants.SHOPPING_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_SHOPPING_APPS;
                        } else {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_OTHER_APPS;
                        }

                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                        appUsageLog.category = AppUsageLog.APP_CATEGORY_OTHER_APPS;
                    }

                    appUsageLog.beginTimeStamp = queryUsageStats.get(i).getFirstTimeStamp();
                    appUsageLog.endTimeStamp = queryUsageStats.get(i).getLastTimeStamp();
                    appUsageLog.lastTimeUsed = queryUsageStats.get(i).getLastTimeUsed();
                    appUsageLog.totalTimeInForeground = foregroundTime;

                    appUsageLogs.add(appUsageLog);

                }

                LogsDatabaseHelper.getInstance().insertAppUsageLogs(appUsageLogs);

                sharedPreferences.edit().putLong(Constants.KEY_LAST_APP_USAGE_LOG_TIME, endTime).commit();
            }
        }
    }

    public static void getAppServiceLogs(Context context) {


        if (Utils.hasUsageStatisticsPermission(context)) {

            UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService(USAGE_STATS_SERVICE);

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -1);
            long endTime = System.currentTimeMillis();
            List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_MONTHLY, cal.getTimeInMillis(), endTime);

            if (queryUsageStats.isEmpty()) {
                return;
            }

            /*ArrayMap<String, UsageStats> aggregatedStats = new ArrayMap<>();
            final int statCount = queryUsageStats.size();
            for (int i = 0; i < statCount; i++) {
                UsageStats newStat = queryUsageStats.get(i);
                UsageStats existingStat = aggregatedStats.get(newStat.getPackageName());
                if (existingStat == null) {
                    aggregatedStats.put(newStat.getPackageName(), newStat);
                } else {
                    existingStat.add(newStat);
                }
            }
            queryUsageStats = new ArrayList<>(aggregatedStats.values());*/

            int size;
            if ((size = queryUsageStats.size()) > 0) {

                List<AppUsageLog> appUsageLogs = new ArrayList<>(size);

                for (int i = 0; i < size; i++) {

                    long foregroundTime = queryUsageStats.get(i).getTotalTimeInForeground() / 1000;
                    String packageName = queryUsageStats.get(i).getPackageName();
                    if (packageName.contains("com.android")
                            || packageName.contains("com.google.android")
                            || packageName.contains("example")
                            || foregroundTime <= 0) {
                        continue;
                    }

                    AppUsageLog appUsageLog = new AppUsageLog();
                    appUsageLog.packageName = packageName;

                    try {
                        ApplicationInfo app = context.getPackageManager().getApplicationInfo(appUsageLog.packageName, 0);
                        String name = context.getPackageManager().getApplicationLabel(app).toString().toLowerCase();

                        if (Constants.BROWSER_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_BROWSER_APPS;
                        } else if (Constants.SOCIAL_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_SOCIAL_APPS;
                        } else if (Constants.CAB_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_CAB_APPS;
                        } else if (Constants.SHOPPING_APPS.contains(name)) {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_SHOPPING_APPS;
                        } else {
                            appUsageLog.category = AppUsageLog.APP_CATEGORY_OTHER_APPS;
                        }

                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                        appUsageLog.category = AppUsageLog.APP_CATEGORY_OTHER_APPS;
                    }

                    appUsageLog.beginTimeStamp = queryUsageStats.get(i).getFirstTimeStamp();
                    appUsageLog.endTimeStamp = queryUsageStats.get(i).getLastTimeStamp();
                    appUsageLog.lastTimeUsed = queryUsageStats.get(i).getLastTimeUsed();
                    appUsageLog.totalTimeInForeground = foregroundTime;

                    appUsageLogs.add(appUsageLog);

                }

                LogsDatabaseHelper.getInstance().deleteAppUsageLogsTable();
                LogsDatabaseHelper.getInstance().insertAppUsageLogs(appUsageLogs);
            }
        }
    }
}
