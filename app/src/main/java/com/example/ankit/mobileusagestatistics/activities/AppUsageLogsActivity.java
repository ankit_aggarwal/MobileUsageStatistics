package com.example.ankit.mobileusagestatistics.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.adapters.AppUsageLogsAdapter;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.databases.LogsDatabaseHelper;
import com.example.ankit.mobileusagestatistics.models.AppUsageLog;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class AppUsageLogsActivity extends AppCompatActivity {

    private List<AppUsageLog> mAppUsageLogs;
    private AppUsageLogsAdapter mAppUsageLogsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_usage_logs);

        ActionBar actionBar = getSupportActionBar();

        String date = getIntent().getStringExtra(Constants.INTENT_EXTRA_DATE);
        int category = getIntent().getIntExtra(Constants.INTENT_EXTRA_APP_CATEGORY, AppUsageLog.APP_CATEGORY_OTHER_APPS);

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setTitle("App Usage logs");

            switch (category) {
                case AppUsageLog.APP_CATEGORY_SOCIAL_APPS:
                    actionBar.setSubtitle("Social Apps " + date);
                    break;

                case AppUsageLog.APP_CATEGORY_BROWSER_APPS:
                    actionBar.setSubtitle("Browser Apps " + date);
                    break;

                case AppUsageLog.APP_CATEGORY_CAB_APPS:
                    actionBar.setSubtitle("Cab Apps " + date);
                    break;

                case AppUsageLog.APP_CATEGORY_SHOPPING_APPS:
                    actionBar.setSubtitle("Shopping Apps " + date);
                    break;

                case AppUsageLog.APP_CATEGORY_OTHER_APPS:
                    actionBar.setSubtitle("Other Apps " + date);
                    break;

                default:
                    break;
            }
        }

        RecyclerView rvAppUsageLogs = (RecyclerView) findViewById(R.id.rv_app_usage_logs);

        if (rvAppUsageLogs != null) {
            rvAppUsageLogs.setLayoutManager(new LinearLayoutManager(this));

            mAppUsageLogs = new ArrayList<>();
            mAppUsageLogsAdapter = new AppUsageLogsAdapter(this, mAppUsageLogs);
            rvAppUsageLogs.setAdapter(mAppUsageLogsAdapter);
        }

        new FetchAppUsageLogsTask(this, category).execute(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class FetchAppUsageLogsTask extends AsyncTask<String, Void, List<AppUsageLog>> {

        private WeakReference<Context> mContextWeakReference;
        private int mCategory;

        public FetchAppUsageLogsTask(Context context, int category) {
            mContextWeakReference = new WeakReference<>(context);
            this.mCategory = category;
        }

        @Override
        protected List<AppUsageLog> doInBackground(String... params) {
            long startTime;
            long endTime;
            try {
                Date date = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(params[0]);

                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(date);

                Calendar newCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                startTime = newCal.getTimeInMillis();

                newCal.add(Calendar.DAY_OF_MONTH, 1);
                endTime = newCal.getTimeInMillis();

                return LogsDatabaseHelper.getInstance().getAppUsageLogs(mCategory, startTime, endTime);
            } catch (ParseException e) {
                e.printStackTrace();
                return new ArrayList<>(0);
            }
        }

        @Override
        protected void onPostExecute(List<AppUsageLog> callLogs) {

            Context context = mContextWeakReference.get();

            if (context != null) {
                ((AppUsageLogsActivity) context).mAppUsageLogs.clear();
                ((AppUsageLogsActivity) context).mAppUsageLogs.addAll(callLogs);
                ((AppUsageLogsActivity) context).mAppUsageLogsAdapter.notifyDataSetChanged();
            }
        }
    }
}
