package com.example.ankit.mobileusagestatistics.commons;

import java.util.HashSet;
import java.util.Set;

public final class Constants {

    private Constants() {
    }

    public static Set<String> BROWSER_APPS;
    public static Set<String> SOCIAL_APPS;
    public static Set<String> CAB_APPS;
    public static Set<String> SHOPPING_APPS;

    public static final String DATE_FORMAT = "MMM d, yyyy";
    public static final String TIME_FORMAT = "hh:mma";

    public static final String SHARED_PREF_NAME = "APP_PREF";
    public static final String KEY_FIRST_TIME_SETUP_DONE = "KEY_FIRST_TIME_SETUP_DONE";
    public static final String KEY_LAST_CALL_LOG_TIME = "KEY_LAST_CALL_LOG_TIME";
    public static final String KEY_LAST_SMS_LOG_TIME = "KEY_LAST_SMS_LOG_TIME";
    public static final String KEY_LAST_APP_USAGE_LOG_TIME = "KEY_LAST_APP_USAGE_LOG_TIME";

    public static final String INTENT_EXTRA_DATE = "date";
    public static final String INTENT_EXTRA_APP_CATEGORY = "appCategory";

    static {
        BROWSER_APPS = new HashSet<>();
        SOCIAL_APPS = new HashSet<>();
        CAB_APPS = new HashSet<>();
        SHOPPING_APPS = new HashSet<>();

        BROWSER_APPS.add("chrome");
        BROWSER_APPS.add("firefox");
        BROWSER_APPS.add("uc");

        SOCIAL_APPS.add("facebook");
        SOCIAL_APPS.add("instagram");
        SOCIAL_APPS.add("telegram");
        SOCIAL_APPS.add("whatsapp");
        SOCIAL_APPS.add("messenger");
        SOCIAL_APPS.add("moments");
        SOCIAL_APPS.add("cherry");
        SOCIAL_APPS.add("youtube");
        SOCIAL_APPS.add("hangouts");
        SOCIAL_APPS.add("everalbum");
        SOCIAL_APPS.add("google+");  //for google plus

        CAB_APPS.add("ola");
        CAB_APPS.add("uber");
        CAB_APPS.add("meru");
        CAB_APPS.add("airport");
        CAB_APPS.add("taxi");       //for taxi for sure

        SHOPPING_APPS.add("amazon");
        SHOPPING_APPS.add("flipkart");
        SHOPPING_APPS.add("snapdeal");
        SHOPPING_APPS.add("grofers");
        SHOPPING_APPS.add("bigbasket");
        SHOPPING_APPS.add("olx");

    }
}
