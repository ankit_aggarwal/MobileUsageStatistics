package com.example.ankit.mobileusagestatistics.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.adapters.SmsLogsAdapter;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.databases.LogsDatabaseHelper;
import com.example.ankit.mobileusagestatistics.models.SMSLog;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class SmsLogsActivity extends AppCompatActivity {

    private List<SMSLog> mSmsLogs;
    private SmsLogsAdapter mSmsLogsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_logs);

        ActionBar actionBar = getSupportActionBar();

        String date = getIntent().getStringExtra(Constants.INTENT_EXTRA_DATE);

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setTitle("Outgoing Sms logs for " + date);
        }

        RecyclerView rvSmsLogs = (RecyclerView) findViewById(R.id.rv_sms_logs);

        if (rvSmsLogs != null) {
            rvSmsLogs.setLayoutManager(new LinearLayoutManager(this));

            mSmsLogs = new ArrayList<>();
            mSmsLogsAdapter = new SmsLogsAdapter(mSmsLogs);
            rvSmsLogs.setAdapter(mSmsLogsAdapter);
        }

        new FetchSmsLogsTask(this).execute(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class FetchSmsLogsTask extends AsyncTask<String, Void, List<SMSLog>> {

        private WeakReference<Context> mContextWeakReference;

        public FetchSmsLogsTask(Context context) {
            mContextWeakReference = new WeakReference<>(context);
        }

        @Override
        protected List<SMSLog> doInBackground(String... params) {
            long startTime;
            long endTime;
            try {
                Date date = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(params[0]);

                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(date);

                Calendar newCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                startTime = newCal.getTimeInMillis();

                newCal.add(Calendar.DAY_OF_MONTH, 1);
                endTime = newCal.getTimeInMillis();

                return LogsDatabaseHelper.getInstance().getSmsLogs(startTime, endTime);
            } catch (ParseException e) {
                e.printStackTrace();
                return new ArrayList<>(0);
            }
        }

        @Override
        protected void onPostExecute(List<SMSLog> callLogs) {

            Context context = mContextWeakReference.get();

            if (context != null) {
                ((SmsLogsActivity) context).mSmsLogs.clear();
                ((SmsLogsActivity) context).mSmsLogs.addAll(callLogs);
                ((SmsLogsActivity) context).mSmsLogsAdapter.notifyDataSetChanged();
            }
        }
    }
}
