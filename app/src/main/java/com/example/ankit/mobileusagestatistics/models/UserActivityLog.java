package com.example.ankit.mobileusagestatistics.models;

import android.support.annotation.NonNull;

import com.example.ankit.mobileusagestatistics.commons.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class UserActivityLog implements Comparable<UserActivityLog> {
    public String date;
    public long callTime;
    public long outgoingSMSCount;

    public Map<Integer, Long> appCategoryAndTimeMap;

    @Override
    public int compareTo(@NonNull UserActivityLog another) {

        try {
            Date d1 = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(date);
            Date d2 = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(another.date);

            long timeDiff = d1.getTime() - d2.getTime();
            if (timeDiff < 0) {
                return 1;
            } else if (timeDiff > 0) {
                return -1;
            } else {
                return 0;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    //public long totalTimeOnMobile;
}
