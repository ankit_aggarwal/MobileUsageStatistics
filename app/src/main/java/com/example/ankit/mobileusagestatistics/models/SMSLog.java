package com.example.ankit.mobileusagestatistics.models;

import com.example.ankit.mobileusagestatistics.databases.LogsContract;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = LogsContract.SMSLog.TABLE_NAME)
public class SMSLog {

    @DatabaseField(id = true, columnName = LogsContract.SMSLog.COLUMN_ID)
    public int id;

    @DatabaseField(columnName = LogsContract.SMSLog.COLUMN_DATE)
    public long date;

    @DatabaseField(columnName = LogsContract.SMSLog.COLUMN_ADDRESS)
    public String address;

    @DatabaseField(columnName = LogsContract.SMSLog.COLUMN_BODY)
    public String body;

    public SMSLog() {
    }

}
