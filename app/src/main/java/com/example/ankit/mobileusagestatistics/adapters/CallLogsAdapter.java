package com.example.ankit.mobileusagestatistics.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.models.CallLog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class CallLogsAdapter extends RecyclerView.Adapter<CallLogsAdapter.CallLogViewHolder> {

    private List<CallLog> mCallLogs;
    private NumberFormat mNumberFormat;

    public CallLogsAdapter(List<CallLog> callLogs) {
        this.mCallLogs = callLogs;
        this.mNumberFormat = new DecimalFormat("#0.00");
    }

    @Override
    public CallLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CallLogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_call_logs, parent, false));
    }

    @Override
    public void onBindViewHolder(CallLogViewHolder holder, int position) {
        holder.tvNumber.setText(mCallLogs.get(position).number);

        switch (mCallLogs.get(position).type) {

            case CallLog.INCOMING_TYPE:
                holder.tvCallType.setText("Incoming Call");
                break;

            case CallLog.MISSED_TYPE:
                holder.tvCallType.setText("Missed Call");
                break;

            case CallLog.OUTGOING_TYPE:
                holder.tvCallType.setText("Outgoing Call");
                break;

            case CallLog.VOICEMAIL_TYPE:
                holder.tvCallType.setText("Voicemail Call");
                break;

            default:
                break;
        }

        long duration = mCallLogs.get(position).duration;
        if (duration < 60) {
            holder.tvDuration.setText(duration + " seconds");
        } else if (duration < 3600) {
            holder.tvDuration.setText(mNumberFormat.format((double) duration / 60) + " minutes");
        } else {
            holder.tvDuration.setText(mNumberFormat.format((double) duration / 3600) + " hrs.");
        }

        holder.tvCallTime.setText(Utils.getTime(mCallLogs.get(position).date, Constants.TIME_FORMAT));
    }

    @Override
    public int getItemCount() {
        return mCallLogs.size();
    }

    public static class CallLogViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNumber;
        public TextView tvCallType;
        public TextView tvDuration;
        public TextView tvCallTime;

        public CallLogViewHolder(View itemView) {
            super(itemView);

            tvNumber = (TextView) itemView.findViewById(R.id.tv_number);
            tvCallType = (TextView) itemView.findViewById(R.id.tv_call_type);
            tvDuration = (TextView) itemView.findViewById(R.id.tv_duration);
            tvCallTime = (TextView) itemView.findViewById(R.id.tv_call_time);
        }
    }
}
