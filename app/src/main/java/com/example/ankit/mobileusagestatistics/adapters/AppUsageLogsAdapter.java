package com.example.ankit.mobileusagestatistics.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.models.AppUsageLog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class AppUsageLogsAdapter extends RecyclerView.Adapter<AppUsageLogsAdapter.CallLogViewHolder> {

    private List<AppUsageLog> mAppUsageLogs;
    private NumberFormat mNumberFormat;
    private PackageManager mPackageManager;

    public AppUsageLogsAdapter(Context context, List<AppUsageLog> appUsageLogs) {
        this.mAppUsageLogs = appUsageLogs;
        this.mNumberFormat = new DecimalFormat("#0.00");
        this.mPackageManager = context.getPackageManager();
    }

    @Override
    public CallLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CallLogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_app_usage_logs, parent, false));
    }

    @Override
    public void onBindViewHolder(CallLogViewHolder holder, int position) {

        try {
            Drawable icon = mPackageManager.getApplicationIcon(mAppUsageLogs.get(position).packageName);
            holder.ivAppIcon.setImageDrawable(icon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            holder.ivAppIcon.setImageResource(R.mipmap.ic_launcher);
        }

        try {
            ApplicationInfo app = mPackageManager.getApplicationInfo(mAppUsageLogs.get(position).packageName, 0);
            String appName = mPackageManager.getApplicationLabel(app).toString().toLowerCase();
            holder.tvAppName.setText(appName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            holder.tvAppName.setText(mAppUsageLogs.get(position).packageName);
        }

        long totalTimeInForeground = mAppUsageLogs.get(position).totalTimeInForeground;
        if (totalTimeInForeground < 60) {
            holder.tvTimeInForeGround.setText(totalTimeInForeground + " seconds");
        } else if (totalTimeInForeground < 3600) {
            holder.tvTimeInForeGround.setText(mNumberFormat.format((double) totalTimeInForeground / 60) + " minutes");
        } else {
            holder.tvTimeInForeGround.setText(mNumberFormat.format((double) totalTimeInForeground / 3600) + " hrs.");
        }

        holder.tvLastTimeUsed.setText(Utils.getTime(mAppUsageLogs.get(position).lastTimeUsed, Constants.TIME_FORMAT));
    }

    @Override
    public int getItemCount() {
        return mAppUsageLogs.size();
    }

    public static class CallLogViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivAppIcon;
        public TextView tvAppName;
        public TextView tvTimeInForeGround;
        public TextView tvLastTimeUsed;

        public CallLogViewHolder(View itemView) {
            super(itemView);

            ivAppIcon = (ImageView) itemView.findViewById(R.id.iv_app_icon);
            tvAppName = (TextView) itemView.findViewById(R.id.tv_app_name);
            tvTimeInForeGround = (TextView) itemView.findViewById(R.id.tv_foreground_in_time);
            tvLastTimeUsed = (TextView) itemView.findViewById(R.id.tv_last_used_time);
        }
    }
}
