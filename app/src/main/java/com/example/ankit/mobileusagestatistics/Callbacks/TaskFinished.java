package com.example.ankit.mobileusagestatistics.Callbacks;

public interface TaskFinished<T> {

    void onSuccess(T data);

    void onFail(String errorMessage);

}
