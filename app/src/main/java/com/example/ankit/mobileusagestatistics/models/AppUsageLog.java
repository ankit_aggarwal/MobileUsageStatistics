package com.example.ankit.mobileusagestatistics.models;

import com.example.ankit.mobileusagestatistics.databases.LogsContract;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = LogsContract.AppUsageLog.TABLE_NAME)
public class AppUsageLog {

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_ID, generatedId = true)
    public int id;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_PACKAGE_NAME)
    public String packageName;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_CATEGORY)
    public int category;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_BEGIN_TIME_STAMP)
    public long beginTimeStamp;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_END_TIME_STAMP)
    public long endTimeStamp;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_LAST_TIME_USED)
    public long lastTimeUsed;

    @DatabaseField(columnName = LogsContract.AppUsageLog.COLUMN_TOTAL_TIME_IN_FOREGROUND)
    public long totalTimeInForeground;

    public static final int APP_CATEGORY_BROWSER_APPS = 1;
    public static final int APP_CATEGORY_SOCIAL_APPS = 2;
    public static final int APP_CATEGORY_CAB_APPS = 3;
    public static final int APP_CATEGORY_SHOPPING_APPS = 4;
    public static final int APP_CATEGORY_OTHER_APPS = 5;

    public AppUsageLog() {
    }

}