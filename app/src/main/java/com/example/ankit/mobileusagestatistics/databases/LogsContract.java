package com.example.ankit.mobileusagestatistics.databases;

public final class LogsContract {

    private LogsContract() {
    }

    public static final String DB_NAME = "LOGS.db";
    public static final int DB_VERSION = 1;

    public static class CallLog {

        public static final String TABLE_NAME = "CallLog";

        public static final String COLUMN_ID = "Id";

        public static final String COLUMN_NUMBER = "number";

        public static final String COLUMN_TYPE = "type";        //outgoing, incoming, missed calls

        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_DURATION = "duration";

    }

    public static class SMSLog {

        public static final String TABLE_NAME = "SMSLog";

        public static final String COLUMN_ID = "Id";

        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_ADDRESS = "address";

        public static final String COLUMN_BODY = "body";

    }

    public static class AppUsageLog {

        public static final String TABLE_NAME = "AppUsageLog";

        public static final String COLUMN_ID = "Id";

        public static final String COLUMN_PACKAGE_NAME = "packageName";

        public static final String COLUMN_CATEGORY = "category";

        public static final String COLUMN_BEGIN_TIME_STAMP = "beginTimeStamp";

        public static final String COLUMN_END_TIME_STAMP = "endTimeStamp";

        public static final String COLUMN_LAST_TIME_USED = "lastTimeUsed";

        public static final String COLUMN_TOTAL_TIME_IN_FOREGROUND = "totalTimeInForeground";

    }

}
