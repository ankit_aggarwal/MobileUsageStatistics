package com.example.ankit.mobileusagestatistics.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.activities.HomeActivity;
import com.example.ankit.mobileusagestatistics.models.AppUsageLog;
import com.example.ankit.mobileusagestatistics.models.UserActivityLog;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class UserActivityAdapter extends RecyclerView.Adapter<UserActivityAdapter.UserActivityViewHolder> {

    private List<UserActivityLog> mUserActivityLogs;
    private NumberFormat mNumberFormat;

    public UserActivityAdapter(List<UserActivityLog> userActivityLogs) {
        this.mUserActivityLogs = userActivityLogs;
        this.mNumberFormat = new DecimalFormat("#0.00");
    }

    @Override
    public UserActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserActivityViewHolder(parent.getContext(), LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_user_activity, parent, false));
    }

    @Override
    public void onBindViewHolder(UserActivityViewHolder holder, int position) {

        long totalTime = 0;

        holder.tvDate.setText(mUserActivityLogs.get(position).date);

        totalTime += mUserActivityLogs.get(position).callTime;
        holder.tvCallTime.setText(String.format(Locale.getDefault(),
                "Total Call Time: %d seconds", mUserActivityLogs.get(position).callTime));

        holder.tvSmsCount.setText(String.format(Locale.getDefault(),
                "Total Sms Count: %d", mUserActivityLogs.get(position).outgoingSMSCount));

        Map<Integer, Long> appCategoryAndTimeMap = mUserActivityLogs.get(position).appCategoryAndTimeMap;
        if (appCategoryAndTimeMap != null) {

            if (appCategoryAndTimeMap.containsKey(AppUsageLog.APP_CATEGORY_SOCIAL_APPS)) {
                long socialAppsTime = appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_SOCIAL_APPS);
                totalTime += socialAppsTime;

                if (socialAppsTime < 60) {
                    holder.tvSocialAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Social Apps Time: %d seconds", socialAppsTime));
                } else if (socialAppsTime < 3600) {
                    holder.tvSocialAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Social Apps Time: %s minutes", mNumberFormat.format((double) socialAppsTime / 60)));
                } else {
                    holder.tvSocialAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Social Apps Time: %s hrs.", mNumberFormat.format((double) socialAppsTime / 3600)));
                }
            } else {
                holder.tvSocialAppsTime.setText("Total Social Apps Time: 0 seconds");
            }

            if (appCategoryAndTimeMap.containsKey(AppUsageLog.APP_CATEGORY_BROWSER_APPS)) {
                long browserAppsTime = appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_BROWSER_APPS);
                totalTime += appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_BROWSER_APPS);

                if (browserAppsTime < 60) {
                    holder.tvBrowserAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Browser Apps Time: %d seconds", browserAppsTime));
                } else if (browserAppsTime < 3600) {
                    holder.tvBrowserAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Browser Apps Time: %s minutes", mNumberFormat.format((double) browserAppsTime / 60)));
                } else {
                    holder.tvBrowserAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Browser Apps Time: %s hrs.", mNumberFormat.format((double) browserAppsTime / 3600)));
                }
            } else {
                holder.tvBrowserAppsTime.setText("Total Browser Apps Time: 0 seconds");
            }

            if (appCategoryAndTimeMap.containsKey(AppUsageLog.APP_CATEGORY_CAB_APPS)) {
                long cabAppsTime = appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_CAB_APPS);
                totalTime += appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_CAB_APPS);

                if (cabAppsTime < 60) {
                    holder.tvCabAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Cab Apps Time: %d seconds", cabAppsTime));
                } else if (cabAppsTime < 3600) {
                    holder.tvCabAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Cab Apps Time: %s minutes", mNumberFormat.format((double) cabAppsTime / 60)));
                } else {
                    holder.tvCabAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Cab Apps Time: %s hrs.", mNumberFormat.format((double) cabAppsTime / 3600)));
                }
            } else {
                holder.tvCabAppsTime.setText("Total Cab Apps Time: 0 seconds");
            }

            if (appCategoryAndTimeMap.containsKey(AppUsageLog.APP_CATEGORY_SHOPPING_APPS)) {
                long shoppingAppsTime = appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_SHOPPING_APPS);
                totalTime += appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_SHOPPING_APPS);

                if (shoppingAppsTime < 60) {
                    holder.tvShoppingAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Shopping Apps Time: %d seconds", shoppingAppsTime));
                } else if (shoppingAppsTime < 3600) {
                    holder.tvShoppingAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Shopping Apps Time: %s minutes", mNumberFormat.format((double) shoppingAppsTime / 60)));
                } else {
                    holder.tvShoppingAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Shopping Apps Time: %s hrs.", mNumberFormat.format((double) shoppingAppsTime / 3600)));
                }
            } else {
                holder.tvShoppingAppsTime.setText("Total Shopping Apps Time: 0 seconds");
            }

            if (appCategoryAndTimeMap.containsKey(AppUsageLog.APP_CATEGORY_OTHER_APPS)) {
                long otherAppsTime = appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_OTHER_APPS);
                totalTime += appCategoryAndTimeMap.get(AppUsageLog.APP_CATEGORY_OTHER_APPS);

                if (otherAppsTime < 60) {
                    holder.tvOtherAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Other Apps Time: %d seconds", otherAppsTime));
                } else if (otherAppsTime < 3600) {
                    holder.tvOtherAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Other Apps Time: %s minutes", mNumberFormat.format((double) otherAppsTime / 60)));
                } else {
                    holder.tvOtherAppsTime.setText(String.format(Locale.getDefault(),
                            "Total Other Apps Time: %s hrs.", mNumberFormat.format((double) otherAppsTime / 3600)));
                }
            } else {
                holder.tvOtherAppsTime.setText("Total Other Apps Time: 0 seconds");
            }
        } else {
            holder.tvSocialAppsTime.setText("Total Social Apps Time: 0 seconds");

            holder.tvBrowserAppsTime.setText("Total Browser Apps Time: 0 seconds");

            holder.tvCabAppsTime.setText("Total Cab Apps Time: 0 seconds");

            holder.tvShoppingAppsTime.setText("Total Shopping Apps Time: 0 seconds");

            holder.tvOtherAppsTime.setText("Total Other Apps Time: 0 seconds");
        }

        if (totalTime < 60) {
            holder.tvTotalTime.setText(String.format(Locale.getDefault(),
                    "Total Time: %d seconds", totalTime));
        } else if (totalTime < 3600) {
            holder.tvTotalTime.setText(String.format(Locale.getDefault(),
                    "Total Time: %s minutes", mNumberFormat.format((double) totalTime / 60)));
        } else {
            holder.tvTotalTime.setText(String.format(Locale.getDefault(),
                    "Total Time: %s hrs.", mNumberFormat.format((double) totalTime / 3600)));
        }
    }

    @Override
    public int getItemCount() {
        return mUserActivityLogs.size();
    }

    public static class UserActivityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private WeakReference<Context> contextWeakReference;

        public TextView tvDate;
        public TextView tvTotalTime;
        public TextView tvCallTime;
        public TextView tvSmsCount;
        public TextView tvSocialAppsTime;
        public TextView tvBrowserAppsTime;
        public TextView tvCabAppsTime;
        public TextView tvShoppingAppsTime;
        public TextView tvOtherAppsTime;

        public UserActivityViewHolder(Context context, View itemView) {
            super(itemView);

            contextWeakReference = new WeakReference<>(context);

            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvTotalTime = (TextView) itemView.findViewById(R.id.tv_total_time);
            tvCallTime = (TextView) itemView.findViewById(R.id.tv_call_time);
            tvSmsCount = (TextView) itemView.findViewById(R.id.tv_sms_count);
            tvSocialAppsTime = (TextView) itemView.findViewById(R.id.tv_social_apps_time);
            tvBrowserAppsTime = (TextView) itemView.findViewById(R.id.tv_browser_apps_time);
            tvCabAppsTime = (TextView) itemView.findViewById(R.id.tv_cab_apps_time);
            tvShoppingAppsTime = (TextView) itemView.findViewById(R.id.tv_shopping_apps_time);
            tvOtherAppsTime = (TextView) itemView.findViewById(R.id.tv_other_apps_time);

            tvCallTime.setOnClickListener(this);
            tvSmsCount.setOnClickListener(this);
            tvSocialAppsTime.setOnClickListener(this);
            tvBrowserAppsTime.setOnClickListener(this);
            tvCabAppsTime.setOnClickListener(this);
            tvShoppingAppsTime.setOnClickListener(this);
            tvOtherAppsTime.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Context context = contextWeakReference.get();

            if (context != null) {
                switch (v.getId()) {
                    case R.id.tv_call_time:
                        ((HomeActivity) context).startCallLogActivity(getAdapterPosition());
                        break;

                    case R.id.tv_sms_count:
                        ((HomeActivity) context).startSmsLogActivity(getAdapterPosition());
                        break;

                    case R.id.tv_social_apps_time:
                        ((HomeActivity) context).startAppLogActivity(AppUsageLog.APP_CATEGORY_SOCIAL_APPS, getAdapterPosition());
                        break;

                    case R.id.tv_browser_apps_time:
                        ((HomeActivity) context).startAppLogActivity(AppUsageLog.APP_CATEGORY_BROWSER_APPS, getAdapterPosition());
                        break;

                    case R.id.tv_cab_apps_time:
                        ((HomeActivity) context).startAppLogActivity(AppUsageLog.APP_CATEGORY_CAB_APPS, getAdapterPosition());
                        break;

                    case R.id.tv_shopping_apps_time:
                        ((HomeActivity) context).startAppLogActivity(AppUsageLog.APP_CATEGORY_SHOPPING_APPS, getAdapterPosition());
                        break;

                    case R.id.tv_other_apps_time:
                        ((HomeActivity) context).startAppLogActivity(AppUsageLog.APP_CATEGORY_OTHER_APPS, getAdapterPosition());
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
