package com.example.ankit.mobileusagestatistics.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ankit.mobileusagestatistics.Callbacks.TaskFinished;
import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.adapters.UserActivityAdapter;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.databases.LogsDatabaseHelper;
import com.example.ankit.mobileusagestatistics.models.UserActivityLog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    /*private List<AppUsageLog> mAppUsageLogs;
    private List<BrowserHistory> mBrowserHistories;
    private List<com.example.ankit.mobileusagestatistics.models.CallLog> mCallLogs;
    private List<SMSLog> mSMSmsLogs;*/
    private List<UserActivityLog> mUserActivityLogs;
    private UserActivityAdapter mUserActivityAdapter;

    private static final int REQUEST_PERMISSIONS = 1;

    /*private static String CHROME_BOOKMARKS_URI =
            "content://com.android.chrome.browser/bookmarks";

    ChromeObserver observer = new ChromeObserver(new Handler());*/

    ProgressDialog mPDHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mPDHome = new ProgressDialog(this);
        mPDHome.setMessage("Please wait..");
        mPDHome.setCancelable(false);

        //set up views
        RecyclerView rvMobileActivities = (RecyclerView) findViewById(R.id.rv_mobile_activities);

        if (rvMobileActivities != null) {
            rvMobileActivities.setLayoutManager(new LinearLayoutManager(this));

            /*mAppUsageLogs = new ArrayList<>();
            mBrowserHistories = new ArrayList<>();
            mCallLogs = new ArrayList<>();
            mSMSmsLogs = new ArrayList<>();*/
            mUserActivityLogs = new ArrayList<>();
            mUserActivityAdapter = new UserActivityAdapter(mUserActivityLogs);

            rvMobileActivities.setAdapter(mUserActivityAdapter);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        List<String> permissions = new ArrayList<>(2);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_CALL_LOG);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_SMS);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_BOOT_COMPLETED)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.RECEIVE_BOOT_COMPLETED);
        }

        if (permissions.size() > 0) {
            ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), REQUEST_PERMISSIONS);
        } else {
            if (Utils.hasUsageStatisticsPermission(this)) {

                mPDHome.show();
                //perform initial setup. this function check if if initial setup is not done then it first populates the db and starts a job scheduler
                Utils.performFirstTimeSetup(this, new TaskFinished<Boolean>() {
                    @Override
                    public void onSuccess(Boolean data) {
                        //fetch analytics data
                        new UserActivityLogsTask(HomeActivity.this).execute();
                    }

                    @Override
                    public void onFail(String errorMessage) {
                        Toast.makeText(HomeActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        mPDHome.dismiss();
                    }
                });
            } else {

                //show user a toast to turn on usage action settings for this app and open settings screen
                Toast.makeText(this, "Please turn on usage access settings for this app", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

                startActivity(intent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                for (int i = 0, size = permissions.length; i < size; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                if (Utils.hasUsageStatisticsPermission(this)) {

                    mPDHome.show();
                    //perform initial setup. this function check if if initial setup is not done then it first populates the db and starts a job scheduler
                    Utils.performFirstTimeSetup(this, new TaskFinished<Boolean>() {
                        @Override
                        public void onSuccess(Boolean data) {
                            //fetch analytics data
                            new UserActivityLogsTask(HomeActivity.this).execute();
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            Toast.makeText(HomeActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            mPDHome.dismiss();
                        }
                    });
                } else {

                    //show user a toast to turn on usage action settings for this app and open settings screen
                    Toast.makeText(this, "Please turn on usage access settings for this app", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

                    startActivity(intent);
                }
                break;
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_results:
                showResults();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/

    private void showResults() {

    }

    public void startCallLogActivity(int position) {
        Intent intent = new Intent(this, CallLogsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_DATE, mUserActivityLogs.get(position).date);
        startActivity(intent);
    }

    public void startSmsLogActivity(int position) {
        Intent intent = new Intent(this, SmsLogsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_DATE, mUserActivityLogs.get(position).date);
        startActivity(intent);
    }

    public void startAppLogActivity(int category, int position) {
        Intent intent = new Intent(this, AppUsageLogsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_APP_CATEGORY, category);
        intent.putExtra(Constants.INTENT_EXTRA_DATE, mUserActivityLogs.get(position).date);
        startActivity(intent);
    }

    public static class UserActivityLogsTask extends AsyncTask<Void, Void, List<UserActivityLog>> {

        private WeakReference<Context> mContextWeakReference;

        public UserActivityLogsTask(Context context) {
            mContextWeakReference = new WeakReference<>(context);
        }

        @Override
        protected List<UserActivityLog> doInBackground(Void... params) {

            Map<String, Long> dateAndCallDurationMap = getCallDetails();
            Map<String, Long> dateAndSmsCountMap = getSentBoxMessages();
            Map<String, Map<Integer, Long>> dateAndAppsCategoryTimeMap = LogsDatabaseHelper.getInstance().getDateAndAppsCategoryTimeMap();

            List<UserActivityLog> userActivityLogs = new ArrayList<>(dateAndCallDurationMap.size());

            for (Map.Entry<String, Long> entry : dateAndCallDurationMap.entrySet()) {
                UserActivityLog activityLog = new UserActivityLog();

                String date = entry.getKey();

                activityLog.date = date;
                activityLog.callTime = entry.getValue();

                if (dateAndSmsCountMap.containsKey(date)) {
                    activityLog.outgoingSMSCount = dateAndSmsCountMap.get(date);
                    dateAndSmsCountMap.remove(date);
                } else {
                    activityLog.outgoingSMSCount = 0;
                }

                if (dateAndAppsCategoryTimeMap.containsKey(date)) {
                    activityLog.appCategoryAndTimeMap = dateAndAppsCategoryTimeMap.get(date);
                    dateAndAppsCategoryTimeMap.remove(date);
                }

                userActivityLogs.add(activityLog);
            }

            for (Map.Entry<String, Long> entry : dateAndSmsCountMap.entrySet()) {
                UserActivityLog activityLog = new UserActivityLog();

                String date = entry.getKey();
                activityLog.date = date;
                activityLog.callTime = 0;
                activityLog.outgoingSMSCount = entry.getValue();

                if (dateAndAppsCategoryTimeMap.containsKey(date)) {
                    activityLog.appCategoryAndTimeMap = dateAndAppsCategoryTimeMap.get(date);
                    dateAndAppsCategoryTimeMap.remove(date);
                }

                userActivityLogs.add(activityLog);
            }

            for (Map.Entry<String, Map<Integer, Long>> entry : dateAndAppsCategoryTimeMap.entrySet()) {
                UserActivityLog activityLog = new UserActivityLog();

                String date = entry.getKey();
                activityLog.date = date;
                activityLog.callTime = 0;
                activityLog.outgoingSMSCount = 0;

                activityLog.appCategoryAndTimeMap = dateAndAppsCategoryTimeMap.get(date);

                userActivityLogs.add(activityLog);
            }

            Collections.sort(userActivityLogs);

            return userActivityLogs;
        }

        @Override
        protected void onPostExecute(List<UserActivityLog> userActivityLogs) {
            Context context = mContextWeakReference.get();
            if (context != null) {
                ((HomeActivity) context).mUserActivityLogs.clear();
                ((HomeActivity) context).mUserActivityLogs.addAll(userActivityLogs);
                ((HomeActivity) context).mUserActivityAdapter.notifyDataSetChanged();
                ((HomeActivity) context).mPDHome.dismiss();
            }
        }

        private Map<String, Long> getCallDetails() {

            Context context = mContextWeakReference.get();

            Map<String, Long> dateAndCallDurationMap = new HashMap<>();

            if (context != null) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG)
                        == PackageManager.PERMISSION_GRANTED) {
                    return LogsDatabaseHelper.getInstance().getDateAndCallDurationMap();
                }
            }

            return dateAndCallDurationMap;
        }

        private Map<String, Long> getSentBoxMessages() {

            Context context = mContextWeakReference.get();

            Map<String, Long> dateAndSmsCountMap = new HashMap<>();

            if (context != null) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS)
                        == PackageManager.PERMISSION_GRANTED) {
                    return LogsDatabaseHelper.getInstance().getDateAndSmsCountMap();
                }
            }

            return dateAndSmsCountMap;
        }
    }

    /*public void getBrowserHistory() {
        Context context = mContextWeakReference.get();

        if (context != null) {
            Cursor mCur = context.getContentResolver()
                    .query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, null, null, null);

            if (mCur != null) {
                mCur.moveToFirst();
                if (mCur.moveToFirst() && mCur.getCount() > 0) {
                    while (!mCur.isAfterLast()) {
                        Log.v("titleIdx", mCur
                                .getString(Browser.HISTORY_PROJECTION_TITLE_INDEX));
                        Log.v("urlIdx", mCur
                                .getString(Browser.HISTORY_PROJECTION_URL_INDEX));
                        mCur.moveToNext();
                    }
                }
                mCur.close();
            }
        }
    }*/

    /*class ChromeObserver extends ContentObserver {
        public ChromeObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange);
            Log.d("onChange: ", uri.toString());

            Cursor cursor = getContentResolver().query(Uri.parse(CHROME_BOOKMARKS_URI), new String[]{"title", "url"},
                    "bookmark = 0", null, null);

            if (cursor != null) {
                cursor.close();
            }

            // process cursor results
        }
    }*/
}
