package com.example.ankit.mobileusagestatistics.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ankit.mobileusagestatistics.R;
import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.models.SMSLog;

import java.util.List;

public class SmsLogsAdapter extends RecyclerView.Adapter<SmsLogsAdapter.SmsLogViewHolder> {

    private List<SMSLog> mSmsLogs;

    public SmsLogsAdapter(List<SMSLog> smsLogs) {
        this.mSmsLogs = smsLogs;
    }

    @Override
    public SmsLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SmsLogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_sms_logs, parent, false));
    }

    @Override
    public void onBindViewHolder(SmsLogViewHolder holder, int position) {
        holder.tvAddress.setText(mSmsLogs.get(position).address);
        holder.tvTime.setText(Utils.getTime(mSmsLogs.get(position).date, Constants.TIME_FORMAT));
        holder.tvBody.setText(mSmsLogs.get(position).body);
    }

    @Override
    public int getItemCount() {
        return mSmsLogs.size();
    }

    public static class SmsLogViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAddress;
        public TextView tvTime;
        public TextView tvBody;

        public SmsLogViewHolder(View itemView) {
            super(itemView);

            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvBody = (TextView) itemView.findViewById(R.id.tv_body);
        }
    }
}
