package com.example.ankit.mobileusagestatistics.commons;

import android.app.Application;
import android.content.Context;

public class LogsApplication extends Application {

    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = this;
    }
}
