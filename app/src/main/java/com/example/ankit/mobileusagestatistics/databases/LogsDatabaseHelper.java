package com.example.ankit.mobileusagestatistics.databases;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ankit.mobileusagestatistics.commons.Constants;
import com.example.ankit.mobileusagestatistics.commons.LogsApplication;
import com.example.ankit.mobileusagestatistics.commons.Utils;
import com.example.ankit.mobileusagestatistics.models.AppUsageLog;
import com.example.ankit.mobileusagestatistics.models.CallLog;
import com.example.ankit.mobileusagestatistics.models.SMSLog;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class LogsDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static LogsDatabaseHelper logsDatabaseHelper;

    private Dao<CallLog, Integer> callLogsDao;
    private Dao<SMSLog, Integer> smsLogsDao;
    private Dao<AppUsageLog, Integer> appUsageLogsDao;

    private LogsDatabaseHelper() {
        super(LogsApplication.APP_CONTEXT, LogsContract.DB_NAME, null, LogsContract.DB_VERSION);
    }

    public static LogsDatabaseHelper getInstance() {
        if (logsDatabaseHelper == null) {
            logsDatabaseHelper = new LogsDatabaseHelper();
        }

        return logsDatabaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, CallLog.class);
            TableUtils.createTable(connectionSource, SMSLog.class);
            TableUtils.createTable(connectionSource, AppUsageLog.class);
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    @Override
    public void close() {
        super.close();
        callLogsDao = null;
        smsLogsDao = null;
        appUsageLogsDao = null;
    }

    private Dao<CallLog, Integer> getCallLogsDao() throws SQLException {
        if (callLogsDao == null) {
            callLogsDao = getDao(CallLog.class);
        }

        return callLogsDao;
    }

    private Dao<SMSLog, Integer> getSmsLogsDao() throws SQLException {
        if (smsLogsDao == null) {
            smsLogsDao = getDao(SMSLog.class);
        }

        return smsLogsDao;
    }

    private Dao<AppUsageLog, Integer> getAppUsageLogsDao() throws SQLException {
        if (appUsageLogsDao == null) {
            appUsageLogsDao = getDao(AppUsageLog.class);
        }

        return appUsageLogsDao;
    }

    //----------------------------------------Insert Queries----------------------------------------//

    public void insertCallLogs(final List<CallLog> callLogs) {

        try {
            final Dao<CallLog, Integer> dao = getCallLogsDao();
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {

                    for (int i = 0, size = callLogs.size(); i < size; i++) {
                        dao.createIfNotExists(callLogs.get(i));
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertSmsLogs(final List<SMSLog> smsLogs) {

        try {
            final Dao<SMSLog, Integer> dao = getSmsLogsDao();
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {

                    for (int i = 0, size = smsLogs.size(); i < size; i++) {
                        dao.createIfNotExists(smsLogs.get(i));
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertAppUsageLogs(final List<AppUsageLog> appUsageLogs) {

        try {
            final Dao<AppUsageLog, Integer> dao = getAppUsageLogsDao();
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {

                    for (int i = 0, size = appUsageLogs.size(); i < size; i++) {
                        dao.create(appUsageLogs.get(i));
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------Select Queries----------------------------------------//
    public Map<String, Long> getDateAndCallDurationMap() {

        Map<String, Long> dateAndCallDurationMap = new HashMap<>();

        try {
            List<CallLog> callLogs = getCallLogsDao().queryBuilder()
                    .orderBy(LogsContract.CallLog.COLUMN_DATE, false)
                    .query();

            int size;
            if (callLogs != null && (size = callLogs.size()) > 0) {

                StringBuilder sb = new StringBuilder();
                sb.append("Call Log :");

                for (int i = 0; i < size; i++) {
                    int callTypeCode = callLogs.get(i).type;
                    String callDate = Utils.getDate(callLogs.get(i).date, Constants.DATE_FORMAT);
                    long callDuration = callLogs.get(i).duration;
                    String callType = null;
                    switch (callTypeCode) {
                        case android.provider.CallLog.Calls.OUTGOING_TYPE:
                            callType = "Outgoing";
                            break;
                        case android.provider.CallLog.Calls.INCOMING_TYPE:
                            callType = "Incoming";
                            break;
                        case android.provider.CallLog.Calls.MISSED_TYPE:
                            callType = "Missed";
                            break;
                    }
                    sb.append("\nPhone Number:--- ").append(callLogs.get(i).number)
                            .append(" \nCall Type:--- ").append(callType)
                            .append(" \nCall Date:--- ").append(callDate)
                            .append(" \nCall duration in sec :--- ").append(callDuration)
                            .append("\n----------------------------------");

                    long totalCallDuration = dateAndCallDurationMap.containsKey(callDate) ?
                            dateAndCallDurationMap.get(callDate) + callDuration : callDuration;
                    dateAndCallDurationMap.put(callDate, totalCallDuration);
                }

                Log.d("call log", sb.toString());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dateAndCallDurationMap;
    }

    public Map<String, Long> getDateAndSmsCountMap() {

        Map<String, Long> dateAndSmsCountMap = new HashMap<>();

        try {
            List<SMSLog> smsLogs = getSmsLogsDao().queryBuilder()
                    .orderBy(LogsContract.CallLog.COLUMN_DATE, false)
                    .query();

            int size;
            if (smsLogs != null && (size = smsLogs.size()) > 0) {
                for (int i = 0; i < size; i++) {
                    String smsDate = Utils.getDate(smsLogs.get(i).date, Constants.DATE_FORMAT);
                    long smsCount = dateAndSmsCountMap.containsKey(smsDate) ? dateAndSmsCountMap.get(smsDate) + 1 : 1;
                    dateAndSmsCountMap.put(smsDate, smsCount);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dateAndSmsCountMap;
    }

    public Map<String, Map<Integer, Long>> getDateAndAppsCategoryTimeMap() {

        Map<String, Map<Integer, Long>> dateAndAppsCategoryTimeMap = new HashMap<>();

        try {

            List<AppUsageLog> appUsageLogs = getAppUsageLogsDao().queryBuilder()
                    .orderBy(LogsContract.AppUsageLog.COLUMN_LAST_TIME_USED, false)
                    .query();

            int size;
            if (appUsageLogs != null && (size = appUsageLogs.size()) > 0) {
                for (int i = 0; i < size; i++) {
                    String date = Utils.getDate(appUsageLogs.get(i).lastTimeUsed, Constants.DATE_FORMAT);

                    Map<Integer, Long> categoryTimeMap;
                    if (dateAndAppsCategoryTimeMap.containsKey(date)) {
                        categoryTimeMap = dateAndAppsCategoryTimeMap.get(date);
                    } else {
                        categoryTimeMap = new HashMap<>();
                    }

                    long time = 0;
                    if (categoryTimeMap.containsKey(appUsageLogs.get(i).category)) {
                        time = categoryTimeMap.get(appUsageLogs.get(i).category);
                    }
                    time += appUsageLogs.get(i).totalTimeInForeground;

                    categoryTimeMap.put(appUsageLogs.get(i).category, time);

                    dateAndAppsCategoryTimeMap.put(date, categoryTimeMap);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dateAndAppsCategoryTimeMap;
    }

    public List<CallLog> getCallLogs(long startTime, long endTime) {
        try {
            return getCallLogsDao().queryBuilder()
                    .where()
                    .between(LogsContract.CallLog.COLUMN_DATE, startTime, endTime)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>(0);
        }
    }

    public List<SMSLog> getSmsLogs(long startTime, long endTime) {
        try {
            return getSmsLogsDao().queryBuilder()
                    .where()
                    .between(LogsContract.SMSLog.COLUMN_DATE, startTime, endTime)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>(0);
        }
    }

    //getting apps based on last time used and not based on begin and end time
    public List<AppUsageLog> getAppUsageLogs(int category, long startTime, long endTime) {
        try {
            return getAppUsageLogsDao().queryBuilder()
                    .where()
                    .eq(LogsContract.AppUsageLog.COLUMN_CATEGORY, category)
                    .and()
                    .between(LogsContract.AppUsageLog.COLUMN_LAST_TIME_USED, startTime, endTime)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>(0);
        }
    }

    //----------------------------------------Delete Queries----------------------------------------//
    //delete app usage logs table and recreate
    public synchronized void deleteAppUsageLogsTable() {
        try {
            TableUtils.dropTable(connectionSource, AppUsageLog.class, true);
            TableUtils.createTable(connectionSource, AppUsageLog.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
