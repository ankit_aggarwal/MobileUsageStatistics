package com.example.ankit.mobileusagestatistics.commons;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.example.ankit.mobileusagestatistics.Callbacks.TaskFinished;
import com.example.ankit.mobileusagestatistics.SchedulerServices.LogService;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {

    /**
     * Gets the time.
     *
     * @param timestamp the timestamp
     * @return the time
     */
    public static String getTime(long timestamp, String format) {
        Date time = new Date(timestamp);
        SimpleDateFormat timeFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        timeFormatter.setDateFormatSymbols(symbols);
        return timeFormatter.format(time);
    }

    /**
     * Gets the date.
     *
     * @param timestamp the timestamp
     * @param format    the format
     * @return the date
     */
    public static String getDate(long timestamp, String format) {
        Date date = new Date(timestamp);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        dateFormatter.setDateFormatSymbols(symbols);
        return dateFormatter.format(date);
    }

    //if first time setup is not done then do it. First populate initial db and then start a job scheduler
    public static void performFirstTimeSetup(final Context context, final TaskFinished<Boolean> callback) {

        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean(Constants.KEY_FIRST_TIME_SETUP_DONE, false)) {

            new AsyncTask<Void, Void, Boolean>() {

                @Override
                protected Boolean doInBackground(Void... params) {
                    //populate initial db
                    populateInitialDb(context, sharedPreferences);

                    return true;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    if (result != null && result) {
                        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

                        //can set setRequiresCharging
                        JobInfo.Builder builder = new JobInfo.Builder(1,
                                new ComponentName(context.getPackageName(), LogService.class.getName()))
                                .setPeriodic(7200000)     //once in every two hours
                                .setPersisted(true);

                        if (jobScheduler.schedule(builder.build()) > 0) {
                            context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                                    .edit()
                                    .putBoolean(Constants.KEY_FIRST_TIME_SETUP_DONE, true)
                                    .commit();
                            callback.onSuccess(true);
                        } else {
                            callback.onFail("Something went wrong during initial setup");
                        }
                    }
                }
            }.execute();
        } else {
            callback.onSuccess(true);
        }
    }

    private static void populateInitialDb(Context context, SharedPreferences sharedPreferences) {
        long lastCallLogTime = sharedPreferences.getLong(Constants.KEY_LAST_CALL_LOG_TIME, 0);
        long lastSmsLogTime = sharedPreferences.getLong(Constants.KEY_LAST_SMS_LOG_TIME, 0);

        //Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.YEAR, -1);

        LogService.addCallLogsInDB(context, lastCallLogTime, sharedPreferences);
        LogService.addSmsLogsInDB(context, lastSmsLogTime, sharedPreferences);
        //LogService.addAppUsageLogsInDB(context, cal.getTimeInMillis(), sharedPreferences);
        LogService.getAppServiceLogs(context);
    }

    public static boolean hasUsageStatisticsPermission(Context context) {

        final UsageStatsManager usageStatsManager = (UsageStatsManager)
                context.getSystemService(Context.USAGE_STATS_SERVICE);
        final List<UsageStats> queryUsageStats = usageStatsManager
                .queryUsageStats(UsageStatsManager.INTERVAL_DAILY, 0, System.currentTimeMillis());

        return !queryUsageStats.isEmpty();
    }
}
