package com.example.ankit.mobileusagestatistics.models;

import com.example.ankit.mobileusagestatistics.databases.LogsContract;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = LogsContract.CallLog.TABLE_NAME)
public class CallLog {

    @DatabaseField(columnName = LogsContract.CallLog.COLUMN_ID, generatedId = true)
    public int id;

    @DatabaseField(columnName = LogsContract.CallLog.COLUMN_NUMBER)
    public String number;

    @DatabaseField(columnName = LogsContract.CallLog.COLUMN_TYPE)
    public int type;

    @DatabaseField(columnName = LogsContract.CallLog.COLUMN_DATE)
    public long date;

    @DatabaseField(columnName = LogsContract.CallLog.COLUMN_DURATION)
    public long duration;

    /** Call log type for incoming calls. */
    public static final int INCOMING_TYPE = 1;
    /** Call log type for outgoing calls. */
    public static final int OUTGOING_TYPE = 2;
    /** Call log type for missed calls. */
    public static final int MISSED_TYPE = 3;
    /** Call log type for voicemails. */
    public static final int VOICEMAIL_TYPE = 4;

    public CallLog() {
    }

}